# -*- encoding: utf-8 -*-
require 'csv'
require './functions.rb'

@OpenFileName = 'mono-record.csv'

#read user name & record from @recordT
@recordT = CSV.table(@OpenFileName)
@recordT.headers

@keyT = @recordT.headers[1..-1]
@rowSumT = getRowSum()

printUserStats()
printOddsPerUser()

  