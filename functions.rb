WinningBorder = 1500

def getRowSum()
  sumT = Array.new(@recordT.length,0)
  @recordT.length.times do |i|
    @keyT.each do |k|
      if @recordT[i][k] != nil then
      sumT[i] += @recordT[i][k]
      end
    end
  end
  return sumT
end

def getPlayers(row)
  ret = 0
  @keyT.each do |k|
    if @recordT[row][k] != nil then
    ret += 1
    end
  end
  return ret
end

def getRank(key, row)
  rank = 1
  @keyT.each do |k|
    if row[k] != nil then
    if row[k]>row[key] then rank += 1 end
    end
  end
  return rank
end

def getRankPt(key, row)
  pt = 25*(5-getRank(key, row))
  if pt < 25 then
  pt = 25
  end
  return pt
end

def getShare(cash, sumCash)
  return (cash.to_f/sumCash.to_f)
end

def getOfficialPoint(cash, row)
  tmp = getShare(cash, @rowSumT[row])*100.0*(getPlayers(row).to_f/4.0)
  if tmp >= 100.0 then
  tmp = 100.0
  end
  if (tmp-25) <= 0 then
    tmp = 0.0
  else
    tmp -= 25.0
  end
  return tmp
end

def printUserStats()
  #if a player has a cash more than $1,499, he is winner.
  csv = CSV.open("PlayerStats.csv", "w")
  csv << ['Player','#match','#win','#top','Top Rate', 'Odds', 'Rank Ave.','Rank stdDev', 'Share Ave.', 'Share stdDev','Cash Sum.', 'Cash Max.', 'Cash Ave.','Cash stdDev','Mono-Pts.', 'Mono-Pts. Ave.', 'Official Pts.', 'Official Pts. Ave.']

  @keyT.each do |key|
    sum = match = win = top = maxC = i = pts = offPts = rSum = sSum = 0
    @recordT[key].each do |cash|
      if cash != nil then
        match += 1
        if cash >= WinningBorder then win += 1 end
        if cash > maxC then maxC = cash end
        if getRank(key, @recordT[i]) == 1 then top += 1 end
        sum += cash
        rSum += getRank(key, @recordT[i])
        sSum += getShare(cash, @rowSumT[i])
        pts += (getRankPt(key, @recordT[i])*getShare(cash, @rowSumT[i]))
        offPts += getOfficialPoint(cash, i)
      end
      i += 1
    end
    ave = sum.to_f/match.to_f
    rAve = rSum.to_f/match.to_f
    sAve = sSum/match.to_f

    stdDevi = rStdD = sStdD = i = 0
    @recordT[key].each do |cash|
      if cash != nil then
        stdDevi += (cash - ave)**2
        rStdD += (getRank(key, @recordT[i]).to_f - rAve)**2
        sStdD += (getShare(cash, @rowSumT[i]).to_f - sAve)**2
      end
      i += 1
    end

    odds = (win.to_f/match.to_f).round(2)
    topRate = (top.to_f/match.to_f).round(2)
    stdDevi = Math.sqrt(stdDevi.to_f/match.to_f).round(2)
    rStdD = Math.sqrt(rStdD.to_f/match.to_f).round(2)
    sStdD = Math.sqrt(sStdD.to_f/match.to_f).round(2)
    ave = ave.round(2)
    rAve = rAve.round(2)
    sAve = sAve.round(2)
    ptAve = (pts/match.to_f).round(1)
    offPtAve = (offPts/match.to_f).round(1)
    pts = pts.round(1)
    offPts = offPts.round(2)
    if match > 0 then
      csv << [key.to_s, match, win, top, topRate, odds, rAve, rStdD, sAve, sStdD, sum, maxC, ave, stdDevi, pts, ptAve, offPts, offPtAve]
    #puts "#{key}  match:#{match} win:#{win} odds:#{odds} Ave:#{rAve} stdDevi:#{rStdD} Ave:#{ave} stdDevi:#{stdDevi} pts:#{pts}  offPts:#{offPts}"
    end
  end
end

def getAIDiff(row, myCash, otherCash)
  ret = 0
  shareOdds = (myCash - otherCash).to_f/@rowSumT[row].to_f
  if shareOdds >= 0.0 then
    ret = Math.log10(1.0+shareOdds*9.0*getPlayers(row).to_f/2.0)*300
  else
    ret = -Math.log10(1.0-shareOdds*9.0*getPlayers(row).to_f/2.0)*300
  end
  return ret
end

def getAIMultiplyer(odds)
  return (((odds-0.5)*2)**2)*2 + 1
end

def printOddsPerUser()
  #put odds with all other players
  #If A has a cash than B, then A win against B.
  csv1 = CSV.open("oddsPerPlayer.csv", "w")
  csv2 = CSV.open("AIPerPlayer.csv", "w")
  csv1 << ['R vs. C']+@keyT
  csv2 << ['R vs. C']+@keyT

  @keyT.each do |myK|
    line = -1
    rOddT = Array.new(@keyT.length,nil)
    rAIT = Array.new(@keyT.length,nil)
    @keyT.each do |othK|
      line += 1
      if myK == othK then next end
      match = win = 0
      ai = 0.0
      row = 0
      @recordT[myK].each do |myC|
        hisC = @recordT[row][othK]
        if myC != nil && hisC != nil then
          ai += getAIDiff(row, myC, hisC)
          match += 1
          if myC >= hisC then
          win += 1
          end
        end
        row += 1
      end
      odds = win.to_f/match.to_f;
      if odds >= 0.5 then
        ai = ai*getAIMultiplyer(odds)
      else
        ai = ai/getAIMultiplyer(odds)
      end
      odds = odds.round(2)
      ai = ai.round(1)
      if match > 0 then
      rOddT[line] = odds
      rAIT[line] = ai
      #        puts "#{myK}->#{othK} odds:#{odds.round(2)} AI:#{ai}"
      end
    end
    csv1 << [myK.to_s]+rOddT
    csv2 << [myK.to_s]+rAIT
  end

  csv1.close
  csv2.close
end
